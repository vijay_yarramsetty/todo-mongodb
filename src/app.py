from fastapi import FastAPI
from .routes import router

# FastAPI application instance
app = FastAPI()

# Adding all API routes of Post Model
app.include_router(router)


# sample hello world response from root url
@app.get('/')
def read_root():
    return {"Hello": "World"}
