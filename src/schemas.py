from pydantic import BaseModel
from typing import Optional


# Incoming data through http request body will be serialized to Python data
class PostIn_Base(BaseModel):
    title: str
    content: str


class PostUp_Base(PostIn_Base):
    title: Optional[str]
    content: Optional[str]


# Response data to be sent through http for client
class Post_Base(PostIn_Base):
    id: str
