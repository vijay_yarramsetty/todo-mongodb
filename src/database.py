from motor import motor_asyncio

# database URL from local machine with default port
URL = "mongodb://localhost:27017"

# Database client using async driver 'motor'
client = motor_asyncio.AsyncIOMotorClient(URL)
# Connecting to 'blog' database
database = client.blog
# Connecting to 'posts' collection in 'blog' database
posts_collection = database.get_collection("posts")


# data conversion from Bson to Python native Dictionary for each document
def post_helper(post) -> dict:
    return {
        "id": str(post["_id"]),
        "title": post["title"],
        "content": post["content"]
    }
