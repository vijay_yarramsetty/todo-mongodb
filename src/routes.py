from .database import posts_collection, post_helper
from .schemas import Post_Base, PostIn_Base, PostUp_Base
from fastapi import APIRouter, HTTPException
from typing import List
from fastapi.responses import JSONResponse
from bson.objectid import ObjectId

# router for all post model apis
router = APIRouter()


# get request for retrieving all posts from db in list of objects format
@router.get('/posts', response_model=List[Post_Base])
async def get_posts():
    posts = []
    async for post in posts_collection.find():
        posts.append(post_helper(post))
    return posts


# post request for storing a blog post on database
@router.post('/posts', response_model=Post_Base)
async def add_post(post: PostIn_Base):
    post_added = await posts_collection.insert_one(post.dict())
    new_post = await posts_collection.find_one({"_id": post_added.inserted_id})
    return post_helper(new_post)


# get request for accessing single post using post id
@router.get('/posts/{post_id}', response_model=Post_Base)
async def get_post(post_id: str):
    post = await posts_collection.find_one({"_id": ObjectId(post_id)})
    if post:
        return post_helper(post)
    raise HTTPException(status_code=404, detail="Post not found")


# update request for updating the contents of blog post
@router.put('/posts/{post_id}', response_model=Post_Base)
async def update_post(post_id: str, post: PostUp_Base):
    req_body = {k: v for k, v in post.dict().items() if v is not None}
    post = await posts_collection.find_one({"_id": ObjectId(post_id)})
    if post:
        if len(req_body) > 0:
            await posts_collection.update_one(
                {"_id": ObjectId(post_id)}, {"$set": req_body}
            )
            upd_post = await posts_collection.find_one({"_id": ObjectId(post_id)})
            return post_helper(upd_post)
        else:
            raise HTTPException(status_code=404, detail="Request body can not be empty")
    raise HTTPException(status_code=404, detail="Post not found to update")


# delete request for deleting the blog post from database
@router.delete('/posts/{post_id}')
async def delete_post(post_id: str):
    post = await posts_collection.find_one({"_id": ObjectId(post_id)})
    if post:
        await posts_collection.delete_one({"_id": ObjectId(post_id)})
        return JSONResponse(status_code=202, content={"message": f"post id {post_id} deleted successfully"})
    raise HTTPException(status_code=404, detail="Post not found to delete")
