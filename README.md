### Project Setup
* MongoDB community edition(version - 4.4 LTE) installed in your machine
* Create a Database & collection for the blog posts from CLI using the following commands
  * activate the MongoDB server `sudo systemctl start mongod`
  * go to MongoDB shell `mongo`
  * create a database -> `use blog`
  * create a collection in that database -> `db.createCollection("posts")`
* clone the remote repository using `git clone git@bitbucket.org:vijay_yarramsetty/todo-mongodb.git`
* Create a virtual environment `python3 -m virtualenv venv`
* Activate the venv `source venv/bin/activate`
* Install the dependencies `pip3 install -r requirements.txt`
* Run the application while being in the root directory from CLI `python3 main.py` or from CLI `uvicorn src.app:app --reload`

### Closing the database connection Deleting the database
* Display all databases `show dbs`
* makesure you are on same database `use blog`
* To delete the database `db.dropDatabase()`
* Close connection `sudo systemctl stop mongod`